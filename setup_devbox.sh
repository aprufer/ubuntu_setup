#!/usr/bin/env bash

# SOURCE: https://askubuntu.com/a/956410
set -eu -o pipefail # fail on error and report it, debug all lines

sudo -n true
test $? -eq 0 || exit 1 "you should have sudo privilege to run this script"

echo installing the must-have pre-requisites
while read -r p ; do sudo apt-get install -y $p ; done < <(cat << "EOF"
    build-essential
    cmake
    gcc
    valgrind
    libcunit1-dev
    libcunit1
    clang
    clang-tidy
    clang-format
    python3
    python-dev
    python3-pip
EOF
)

echo installing the nice-to-have pre-requisites
echo you have 5 seconds to proceed ...
echo or
echo hit Ctrl+C to quit
echo -e "\n"
sleep 6

sudo apt-get install -y tig
